module.exports = {
  extends: "../../.eslintrc.js",
  ignorePatterns: [
    "node_modules/**/*"
  ],
  overrides: [
    {
      files: [
        "src/**/*.ts"
      ],
      parserOptions: {
        project: [
          "projects/ngx-i8-angularfire/tsconfig.lib.json",
          "projects/ngx-i8-angularfire/tsconfig.spec.json"
        ],
        createDefaultProgram: true
      },
      rules: {
      }
    },
  ]
}
