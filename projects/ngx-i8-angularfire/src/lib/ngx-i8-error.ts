export class NgxI8Error extends Error {
  protected code: string;
  protected data: { [key: string]: any } | null;

  constructor(code: string, data: { [key: string]: any } | null = null, message?: string) {
    let errorMessage = `${code}:`;
    if (message) {
      errorMessage += ` ${message}`;
    }
    if (data) {
      errorMessage += ` ${JSON.stringify(data)}`;
    }
    super(errorMessage);
    this.code = code;
    this.data = data;
  }
}
