import { Firestore } from "@angular/fire/firestore";
import { NgxI8AbstractGenericDao } from "ngx-i8-angularfire";

import { AuthService } from "./auth/auth.service";

// TODO Move to NgxI8AngularFire
export class NgxI8AbstractPrivateDao<T> extends NgxI8AbstractGenericDao<T> {
  protected userId: string;

  constructor(firestore: Firestore, protected authSrvc: AuthService, path: string) {
    super(firestore, path);
    this.userId = this.authSrvc.getCurrentUserId(true);
  }

  buildCollectionPath(baseCollectionPath: string, collectionPathOpts?: { [key: string]: string }) {
    collectionPathOpts = collectionPathOpts || {};
    collectionPathOpts[":userId"] = this.userId;
    return super.buildCollectionPath(baseCollectionPath, collectionPathOpts);
  }
}
