import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentData,
  DocumentReference,
  DocumentSnapshot,
  FieldValue,
  Firestore,
  getDoc,
  getDocs,
  onSnapshot,
  Query,
  query,
  QueryConstraint,
  QuerySnapshot,
  setDoc,
  SetOptions,
  updateDoc,
} from "@angular/fire/firestore";
import { Observable, Subject } from "rxjs";
import { shareReplay } from "rxjs/operators";

export class NgxI8FirestoreWrapper {
  constructor(protected firestore: Firestore) {}

  public documentRef(path: string): DocumentReference<DocumentData> {
    return doc(this.firestore, path);
  }

  public collectionRef(path: string): CollectionReference<DocumentData> {
    return collection(this.firestore, path);
  }

  public async addDoc(
    collectionRef: CollectionReference<DocumentData>,
    data: { [x: string]: FieldValue | Partial<unknown> }
  ): Promise<DocumentReference<DocumentData>> {
    return addDoc(collectionRef, data);
  }

  public async setDoc(
    documentRef: DocumentReference<DocumentData>,
    data: { [x: string]: FieldValue | Partial<unknown> },
    opts?: SetOptions
  ): Promise<void> {
    return setDoc(documentRef, data, opts || {});
  }

  public async updateDoc(
    documentRef: DocumentReference<DocumentData>,
    fields: { [x: string]: FieldValue | Partial<unknown> | undefined }
  ): Promise<void> {
    return updateDoc(documentRef, fields);
  }

  public async deleteDoc(documentRef: DocumentReference<any>): Promise<void> {
    return deleteDoc(documentRef);
  }

  public async getDoc(documentRef: DocumentReference): Promise<DocumentSnapshot<DocumentData>> {
    return getDoc(documentRef);
  }

  public getDoc$(
    documentRef: DocumentReference<DocumentData>
  ): Observable<DocumentSnapshot<DocumentData>> {
    const subject: Subject<DocumentSnapshot<DocumentData>> = new Subject();
    onSnapshot(documentRef, subject);
    this.getDoc(documentRef)
      .then((document) => subject.next(document))
      .catch((err) => subject.error(err));
    return subject.pipe(shareReplay(1));
  }

  public async getDocs(
    collectionRef: CollectionReference<DocumentData>,
    queryConstraints?: QueryConstraint[]
  ): Promise<QuerySnapshot<DocumentData>> {
    let docQuery: any = collectionRef;
    if (queryConstraints) {
      docQuery = query(collectionRef, ...queryConstraints);
    }
    return getDocs(docQuery);
  }

  public getDocs$(
    collectionRef: CollectionReference<DocumentData>,
    queryConstraints?: QueryConstraint[]
  ): Observable<QuerySnapshot<DocumentData>> {
    const subject: Subject<QuerySnapshot<DocumentData>> = new Subject();
    let collectionOrQuery: CollectionReference<DocumentData> | Query<DocumentData> = collectionRef;
    if (queryConstraints) {
      collectionOrQuery = query(collectionRef, ...queryConstraints);
    }
    onSnapshot(collectionOrQuery, subject);
    this.getDocs(collectionRef, queryConstraints)
      .then((documents) => subject.next(documents))
      .catch((err) => subject.error(err));
    return subject.pipe(shareReplay(1));
  }
}
