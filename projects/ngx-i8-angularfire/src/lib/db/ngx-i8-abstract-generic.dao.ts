import {
  CollectionReference,
  DocumentData,
  DocumentReference,
  DocumentSnapshot,
  Firestore,
  PartialWithFieldValue,
  QueryConstraint,
  SetOptions,
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { filter, map } from "rxjs/operators";

import { NgxI8Error } from "../ngx-i8-error";
import { NgxI8FirestoreWrapper } from "./ngx-i8-firestore.wrapper";

export interface NgxI8IdObject {
  id?: string;
}

/**
 * This class abstracts most of the complexity of dealing with Firebase Collections.
 *
 * Usage:
 * ```
 * class MyDao extends NgxI8AbstractDao<MyObjectInterface> {
 *   constructor(
 *     afs: AngularFirestore,
 *     afAnalytics: AngularFireAnalytics,
 *     logSrvc: NgxI8LogService)
 *   {
 *     super(afs, afAnalytics, "collectionName", logSrvc);
 *   }
 * }
 * ```
 */
export abstract class NgxI8AbstractGenericDao<T extends NgxI8IdObject> {
  protected i8fs: NgxI8FirestoreWrapper;

  constructor(firestore: Firestore, protected baseCollectionPath: string) {
    this.i8fs = new NgxI8FirestoreWrapper(firestore);
  }

  protected buildCollectionPath(
    baseCollectionPath: string,
    collectionPathOpts?: { [key: string]: string }
  ): string {
    let collectionPath: string = baseCollectionPath;
    for (const entry of Object.entries(collectionPathOpts || {})) {
      let key = entry[0];
      if (!key.startsWith(":")) {
        key = ":" + key;
      }
      collectionPath = collectionPath.replace(key, entry[1]);
    }
    if (collectionPath.indexOf(":") !== -1) {
      throw new NgxI8Error("invalid-collection-path", { collectionPath });
    }
    return collectionPath;
  }

  protected buildDocumentPath(
    id: string,
    baseCollectionPath: string,
    collectionPathOpts?: { [key: string]: string }
  ): string {
    const collectionPath = this.buildCollectionPath(baseCollectionPath, collectionPathOpts);
    return `${collectionPath}/${id}`;
  }

  /**
   * Returns a reference for a collection.
   */
  protected collectionRef(): CollectionReference<DocumentData> {
    const path = this.buildCollectionPath(this.baseCollectionPath);
    return this.i8fs.collectionRef(path);
  }

  /**
   * Returns a reference for a document.
   */
  protected documentRef(id: string): DocumentReference<DocumentData> {
    const path: string = this.buildDocumentPath(id, this.baseCollectionPath);
    return this.i8fs.documentRef(path);
  }
  //#endregion

  //#region Data Manipulation -------------------------------------------------
  /**
   * Returns the id of the object or null.
   */
  protected getId(t: T): string | null {
    return t.id || null;
  }

  /**
   * Returns a new object without the id to be used in the {@link save}
   *
   */
  protected extractData(object: { [key: string]: any }): { [key: string]: any } {
    const data = Object.assign({}, object);
    delete data.id;
    return data;
  }

  /**
   * Generates an id based on the document data to be used in the {@link save}.
   * By default it returns null and the id will be generated by Firebase.
   *
   * *You must overwrite this method if you need an data based id (ie email).*
   */
  protected generateId(t: T): string | null {
    return null;
  }

  //#endregion

  //#region CRUD --------------------------------------------------------------

  /**
   * Creates the object only if doesn't exist.
   *
   * @throw err.code: document-already-exist
   * @returns saved object with updated id.
   * @see generateId
   */
  async create(t: T): Promise<T> {
    const id = this.getId(t);
    if (id && (await this.exists(id))) {
      throw new NgxI8Error("document-already-exist", { create: id });
    }
    return this.save(t);
  }

  /**
   * Save the object. It generates a new id if it doesn't exists.
   *
   * @param t object to save
   * @returns saved object with updated id.
   * @see generateId
   */
  async save(t: T): Promise<T> {
    const id = this.getId(t) || this.generateId(t);
    const data = this.extractData(t);
    if (id) {
      const documentRef = this.documentRef(id);
      await this.i8fs.setDoc(documentRef, data);
      t.id = id;
    } else {
      const collectionRef = this.collectionRef();
      const documentRef = await this.i8fs.addDoc(collectionRef, data);
      t.id = documentRef.id;
    }
    return t;
  }

  /**
   * Update the object only if it already exists.
   *
   * @param t Object to update with id.
   * @returns Updated object
   */
  async update(t: T): Promise<T> {
    const id = this.getId(t);
    if (id) {
      const documentRef = this.documentRef(id);
      const data = this.extractData(t);
      await this.i8fs.updateDoc(documentRef, data);
      return t;
    } else {
      throw new NgxI8Error("id-is-required", { id });
    }
  }

  /**
   * Perform partial updates in the document
   *
   * @param id Object id
   * @param fields fields with values or operations to perform
   * @param opts setDoc options. Default: { merge: true }
   * @see https://firebase.google.com/docs/reference/js/firestore_.md#setdoc
   */
  async set(
    id: string,
    fields: PartialWithFieldValue<T> | { [key: string]: any },
    opts?: SetOptions
  ): Promise<void> {
    const data = this.extractData(fields);
    const documentRef = this.documentRef(id);
    await this.i8fs.setDoc(documentRef, data, opts || { merge: true });
  }

  /**
   * Delete a document from the collection
   *
   * @param id Object id
   */
  async delete(id: string): Promise<void> {
    const documentRef = this.documentRef(id);
    return await this.i8fs.deleteDoc(documentRef);
  }

  /**
   *
   * @param id Check if a document exists.
   * @returns if the document exists.
   */
  async exists(id: string): Promise<boolean> {
    const documentRef = this.documentRef(id);
    const doc = await this.i8fs.getDoc(documentRef);
    return doc.exists();
  }

  /**
   * Return an observable for a document (shareReplay(1) by default)
   *
   * @param id Object id
   * @returns Observable of the document.
   */
  get$(id: string): Observable<T | null> {
    const documentRef = this.documentRef(id);
    return this.i8fs.getDoc$(documentRef).pipe(map(this.handleDocumentSnapshot));
  }

  /**
   * @param id Object id
   * @returns Snapshot of a document
   */
  async get(id: string): Promise<T | null> {
    const documentRef = this.documentRef(id);
    const documentSnapshot = await this.i8fs.getDoc(documentRef);
    return this.handleDocumentSnapshot(documentSnapshot);
  }

  list$(queryConstraints?: QueryConstraint[]): Observable<T[]> {
    const collectionRef = this.collectionRef();
    return this.i8fs.getDocs$(collectionRef, queryConstraints).pipe(
      map((querySnapshot) => {
        return querySnapshot.docs.map(this.handleDocumentSnapshot);
      }),
      filter((t) => !!t)
    ) as Observable<T[]>;
  }

  /**
   * Returns a promise based on {@link list$} with the last/next value.
   *
   * @param collectionPathOpts {@link QueryPathOpts} or
   * {@link getDefaultQueryPathOpts}
   */
  async list(queryConstraints?: QueryConstraint[]): Promise<T[]> {
    const collectionRef = this.collectionRef();
    const collectionSnapshot = await this.i8fs.getDocs(collectionRef, queryConstraints);
    return collectionSnapshot.docs.map(this.handleDocumentSnapshot).filter((t) => !!t) as T[];
  }

  private handleDocumentSnapshot(documentSnapshot: DocumentSnapshot<DocumentData>): T | null {
    const data = documentSnapshot.data();
    if (data) {
      const id = documentSnapshot.id;
      return { id, ...data } as T;
    } else {
      return null;
    }
  }
}
