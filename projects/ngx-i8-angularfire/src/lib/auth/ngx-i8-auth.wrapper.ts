import {
  ActionCodeInfo,
  applyActionCode,
  Auth,
  AuthCredential,
  authState,
  checkActionCode,
  confirmPasswordReset,
  createUserWithEmailAndPassword,
  fetchSignInMethodsForEmail,
  reauthenticateWithCredential,
  sendEmailVerification,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut,
  updatePassword,
  User,
  UserCredential,
} from "@angular/fire/auth";
import { firstValueFrom, Observable } from "rxjs";
import { shareReplay } from "rxjs/operators";

/**
 * https://firebase.google.com/docs/reference/js/auth.md
 * TODO linkWithCredential(user, credential)
 * TODO signInWithCredential(credential)
 * TODO unlink(user, providerId)
 * TODO updateEmail(newEmail)
 * TODO updateProfile({})
 * TODO verifyBeforeUpdateEmail(user, newEmail, actionCodeSettings)
 * TODO useDeviceLanguage()
 * TODO deleteUser()
 */
export class NgxI8AuthWrapper {
  constructor(protected auth: Auth) {}

  authState$(): Observable<User | null> {
    return authState(this.auth).pipe(shareReplay(1));
  }

  async currentUser(): Promise<User | null> {
    return await firstValueFrom(this.authState$());
  }

  signInWithEmailAndPassword(email: string, password: string): Promise<UserCredential> {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  signOut(): Promise<void> {
    return signOut(this.auth);
  }

  createUserWithEmailAndPassword(email: string, password: string): Promise<UserCredential> {
    return createUserWithEmailAndPassword(this.auth, email, password);
  }

  sendEmailVerification(user: User): Promise<void> {
    // TODO iOS and Android https://firebase.google.com/docs/reference/js/auth.actioncodeinfo
    return sendEmailVerification(user);
  }

  sendPasswordResetEmail(email: string): Promise<void> {
    return sendPasswordResetEmail(this.auth, email);
  }

  applyActionCode(oobCode: string): Promise<void> {
    return applyActionCode(this.auth, oobCode);
  }

  checkActionCode(oobCode: string): Promise<ActionCodeInfo> {
    return checkActionCode(this.auth, oobCode);
  }

  confirmPasswordReset(oobCode: string, password: string): Promise<void> {
    return confirmPasswordReset(this.auth, oobCode, password);
  }

  reauthenticateWithCredential(user: User, credential: AuthCredential): Promise<UserCredential> {
    return reauthenticateWithCredential(user, credential);
  }

  updatePassword(user: User, password: string): Promise<void> {
    return updatePassword(user, password);
  }

  fetchSignInMethodsForEmail(email: string): Promise<string[]> {
    return fetchSignInMethodsForEmail(this.auth, email);
  }
}
