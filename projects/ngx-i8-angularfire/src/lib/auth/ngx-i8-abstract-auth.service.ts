import { ActionCodeInfo, Auth, EmailAuthProvider, User, UserCredential } from "@angular/fire/auth";
import { Observable } from "rxjs";

import { NgxI8Error } from "../ngx-i8-error";
import { NgxI8AuthWrapper } from "./ngx-i8-auth.wrapper";

export class NgxI8AbstractAuthService {
  protected i8Auth: NgxI8AuthWrapper;
  protected user$: Observable<User | null>;
  protected currentUser: User | null = null;

  constructor(auth: Auth) {
    this.i8Auth = new NgxI8AuthWrapper(auth);
    this.user$ = this.i8Auth.authState$();
    this.user$.subscribe((user) => (this.currentUser = user));
  }

  // #region Current user
  getUser$(): Observable<User | null> {
    return this.user$;
  }

  /**
   * This function might return the wrong state. You might have to call @see isAuthenticated first.
   *
   * @param failOnNull instead of null, raise an Error.
   * @returns The current user or null
   */
  getCurrentUser(failOnNull = false): User | null {
    if (this.currentUser) {
      return this.currentUser;
    } else if (failOnNull) {
      throw new NgxI8Error("not-signed-in");
    }
    return null;
  }

  /**
   * @see getCurrentUser
   * @param failOnNull instead of null, raise an Error.
   * @returns The current user id or null
   */
  getCurrentUserId(failOnNull = false): string | null {
    const user = this.getCurrentUser(failOnNull);
    if (user) {
      return user.uid;
    } else {
      return null;
    }
  }

  async isAuthenticated(): Promise<boolean> {
    return !!(await this.i8Auth.currentUser());
  }
  // #endregion

  // #region Sign in
  async signInWithEmailAndPassword(email: string, password: string): Promise<UserCredential> {
    try {
      return await this.i8Auth.signInWithEmailAndPassword(email, password);
    } catch (err) {
      await this.i8Auth.signOut();
      throw err;
    }
  }

  async fetchSignInMethodsForEmail(email: string) {
    return this.i8Auth.fetchSignInMethodsForEmail(email);
  }

  async signOut(): Promise<void> {
    return this.i8Auth.signOut();
  }
  // #endregion

  // #region Create user
  async createUserWithEmailAndPassword(email: string, password: string): Promise<UserCredential> {
    const creds = await this.i8Auth.createUserWithEmailAndPassword(email, password);
    await this.i8Auth.sendEmailVerification(creds.user);
    return creds;
    // TODO iOS and Android https://firebase.google.com/docs/reference/js/auth.actioncodeinfo
  }

  /**
   * Validates the user email/other mechanism
   *
   * @param oobCode
   */
  applyActionCode(oobCode: string): Promise<void> {
    return this.i8Auth.applyActionCode(oobCode);
  }
  checkActionCode(oobCode: string): Promise<ActionCodeInfo> {
    return this.i8Auth.checkActionCode(oobCode);
  }

  // #endregion

  // #region Password
  recoverPassword(email: string): Promise<void> {
    return this.i8Auth.sendPasswordResetEmail(email);
  }

  async changePassword(password: string): Promise<void> {
    const user = (await this.getCurrentUser(true)) as User;
    return this.i8Auth.updatePassword(user, password);
  }

  async checkPassword(password: string): Promise<boolean> {
    const user = (await this.getCurrentUser(true)) as User;
    const credential = EmailAuthProvider.credential(user.email || "", password);
    const result = await this.i8Auth.reauthenticateWithCredential(user, credential);
    return !!result;
  }

  confirmPasswordReset(oobCode: string, password: string): Promise<void> {
    return this.i8Auth.confirmPasswordReset(oobCode, password);
  }

  // #endregion
}
