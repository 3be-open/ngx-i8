/*
 * Public API Surface of ngx-i8-angularfire
 */

export { NgxI8AbstractAuthService } from "./lib/auth/ngx-i8-abstract-auth.service";
export { NgxI8AuthWrapper } from "./lib/auth/ngx-i8-auth.wrapper";

export { NgxI8AbstractGenericDao, NgxI8IdObject } from "./lib/db/ngx-i8-abstract-generic.dao";
export { NgxI8FirestoreWrapper } from "./lib/db/ngx-i8-firestore.wrapper";

export { NgxI8Error } from "./lib/ngx-i8-error";
