module.exports = {
  extends: "../../.eslintrc.js",
  ignorePatterns: [
    "node_modules/**/*"
  ],
  overrides: [
    {
      files: [
        "src/**/*.ts"
      ],
      parserOptions: {
        project: [
          "projects/ngx-i8-ionic/tsconfig.lib.json",
          "projects/ngx-i8-ionic/tsconfig.spec.json"
        ],
        createDefaultProgram: true
      },
      rules: {
        "sonarjs/no-ignored-return": 0,
        "@angular-eslint/directive-selector": [
          "error",
          {
            type: "attribute",
            prefix: "ngxI8",
            style: "camelCase"
          }
        ],
        "@angular-eslint/component-selector": [
          "error",
          {
            type: "element",
            prefix: "ngx-i8",
            style: "kebab-case"
          }
        ]
      }
    },
  ]
}
