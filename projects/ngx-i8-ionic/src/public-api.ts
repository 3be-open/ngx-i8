/*
 * Public API Surface of ngx-i8-validation
 */
// Module
export { NgxI8IonicModule } from "./lib/ngx-i8-ionic.module";

export { NgxI8AuthTemplateComponent } from "./lib/auth-template/ngx-i8-auth-template.component";
export { ngxI8AreEqualValidator } from "./lib/validators/ngx-i8-are-equal.validator";
export { NgxI8ButtonsDirective } from "./lib/buttons-directive/buttons.directive";
export { NgxI8CharCountDirective } from "./lib/char-count-directive/ngx-i8-char-count.directive";
export { NgxI8DialogService } from "./lib/dialog-service/ngx-i8-dialog.service";
export { NgxI8PasswordUnmaskDirective } from "./lib/password-unmask-directive/ngx-i8-password-unmask.directive";
export { NgxI8ValidationMessagesDirective } from "./lib/validation-messages-directive/ngx-i8-validation-messages.directive";
export { NgxI8AutoCompleteDirective } from "./lib/auto-complete-directive/ngx-i8-auto-complete.directive";
