import { Directive, ElementRef, HostListener, OnInit, Renderer2 } from "@angular/core";
import { NgControl } from "@angular/forms";

/**
 * Creates a button in the input to turn on and off the password masking.
 *
 * ```html
 * <ion-input
 *    name="input-new-password-1"
 *    type="password"
 *    formControlName="newPassword1"
 *    autocomplete="new-password"
 *    ngxI8PasswordUnmask
 * ></ion-input>
 * ```
 */
@Directive({
  selector: "ion-input[ngxI8PasswordUnmask]",
})
export class NgxI8PasswordUnmaskDirective implements OnInit {
  private icon: any;
  private isKeyPressed = false;

  constructor(
    private element: ElementRef,
    private control: NgControl,
    private renderer: Renderer2
  ) {}

  @HostListener("keyup") keyPressed() {
    this.isKeyPressed = this.isKeyPressed || this.control?.control?.dirty || false;
  }

  ngOnInit(): void {
    this.buildToggleElement();
    this.isKeyPressed = false;
  }

  buildToggleElement() {
    const nativeElement = this.element.nativeElement;
    this.icon = this.renderer.createElement("ion-icon");
    this.renderer.listen(this.icon, "click", this.toggle.bind(this));
    this.renderer.setAttribute(this.icon, "slot", "end");
    this.renderer.setAttribute(this.icon, "class", "ion-padding-top");
    this.renderer.setAttribute(this.icon, "name", "eye-outline");
    this.renderer.insertBefore(nativeElement.parentElement, this.icon, nativeElement);
  }

  toggle() {
    if (this.isKeyPressed) {
      const nativeElement = this.element.nativeElement;
      let type = nativeElement.children[0].type;
      if (type === "password") {
        type = "text";
        this.renderer.setAttribute(this.icon, "color", "primary");
      } else {
        type = "password";
        this.renderer.removeAttribute(this.icon, "color");
      }
      nativeElement.children[0].type = type;
    }
  }
}
