import { ElementRef, Renderer2 } from "@angular/core";

import { mockNgRenderer2 } from "../char-count-directive/ngx-i8-char-count.directive.spec";
import { NgxI8PasswordUnmaskDirective } from "./ngx-i8-password-unmask.directive";

describe("NgxI8PasswordUnmaskDirective", () => {
  let directive: NgxI8PasswordUnmaskDirective;
  let element: ElementRef;
  let control: any;
  let renderer: Renderer2;

  beforeEach(() => {
    element = {
      nativeElement: {
        children: [{ type: "password" }],
        parentElement: { parent: 1 },
      },
    };
    control = { value: "", control: { dirty: false } };
    renderer = mockNgRenderer2();
    directive = new NgxI8PasswordUnmaskDirective(element, control, renderer);
  });

  describe("ngInit", () => {
    it("should build the element and set the keyPressed to false", () => {
      (renderer.createElement as jasmine.Spy).and.returnValue("icon");
      directive.ngOnInit();

      expect((directive as any).icon).toBeTruthy();
      expect((directive as any).isKeyPressed).toBeFalse();
      expect(renderer.listen).toHaveBeenCalledWith("icon", "click", jasmine.any(Function));
    });
  });

  describe("keyPressed", () => {
    it("should be false if the keyPressed is false and control is not dirty", () => {
      (directive as any).isKeyPressed = false;
      control.control.dirty = false;
      directive.keyPressed();

      expect((directive as any).isKeyPressed).toBeFalse();
    });

    it("should be true if the control is dirty", () => {
      (directive as any).isKeyPressed = false;
      control.control.dirty = true;
      directive.keyPressed();

      expect((directive as any).isKeyPressed).toBeTrue();
    });

    it("should be true if the keyPressed is true", () => {
      (directive as any).isKeyPressed = true;
      control.control.dirty = false;
      directive.keyPressed();

      expect((directive as any).isKeyPressed).toBeTrue();
      control.control.dirty = true;
      directive.keyPressed();

      expect((directive as any).isKeyPressed).toBeTrue();
    });
  });

  describe("toggle", () => {
    it("should do nothing if keypressed is false", () => {
      (directive as any).isKeyPressed = false;
      const initialType = element.nativeElement.children[0].type;
      directive.toggle();

      expect(element.nativeElement.children[0].type).toEqual(initialType);
      expect(renderer.setAttribute).not.toHaveBeenCalled();
      expect(renderer.removeAttribute).not.toHaveBeenCalled();
    });

    it("should set attribute color of the icon and update the type to text when type is password", () => {
      (directive as any).isKeyPressed = true;
      (directive as any).icon = true;
      element.nativeElement.children[0].type = "password";
      directive.toggle();

      expect(element.nativeElement.children[0].type).toEqual("text");
      expect(renderer.setAttribute).toHaveBeenCalledWith(
        (directive as any).icon,
        "color",
        "primary"
      );

      expect(renderer.removeAttribute).not.toHaveBeenCalled();
    });

    it("should remove attribute color of the icon and update the type to password when type is text", () => {
      (directive as any).isKeyPressed = true;
      (directive as any).icon = true;
      element.nativeElement.children[0].type = "text";
      directive.toggle();

      expect(element.nativeElement.children[0].type).toEqual("password");
      expect(renderer.setAttribute).not.toHaveBeenCalled();
      expect(renderer.removeAttribute).toHaveBeenCalledWith((directive as any).icon, "color");
    });
  });
});
