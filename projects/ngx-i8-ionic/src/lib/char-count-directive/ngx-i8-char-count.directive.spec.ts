import { ElementRef, Renderer2 } from "@angular/core";
import { AbstractControl, NgControl } from "@angular/forms";
import { faker } from "@faker-js/faker";

import { NgxI8CharCountDirective } from "./ngx-i8-char-count.directive";

export function mockNgRenderer2() {
  return jasmine.createSpyObj("Renderer2", [
    "appendChild",
    "createElement",
    "createText",
    "insertBefore",
    "listen",
    "removeAttribute",
    "setAttribute",
    "setValue",
  ]);
}

describe("NgxI8CharCountDirective", () => {
  let el: ElementRef;
  let control: NgControl;
  let renderer: Renderer2;
  let directive: NgxI8CharCountDirective;

  beforeEach(() => {
    el = {
      nativeElement: {
        localName: "ion-input",
        children: [{ child: 1 }],
        parentElement: { parentElement: { grandParent: 1 } },
      },
    };
    control = {
      value: "original",
      control: {
        patchValue: jasmine.createSpy("patchValue"),
        validator: jasmine.createSpy("validator"),
      } as unknown as AbstractControl,
    } as unknown as NgControl;

    renderer = mockNgRenderer2();
    directive = new NgxI8CharCountDirective(el, control, renderer);
  });

  describe("ngOnInit", () => {
    it("should call getInput, createCountElement and getMaxLength", async () => {
      const getInputResult = { value: "1" };
      const createCountElementResult = { value: "2" };
      const getMaxLengthResult = 100;
      (directive as any).getInput = jasmine
        .createSpy()
        .and.returnValue(Promise.resolve(getInputResult));
      (directive as any).createCountElement = jasmine
        .createSpy()
        .and.returnValue(createCountElementResult);
      (directive as any).getMaxLength = jasmine.createSpy().and.returnValue(getMaxLengthResult);

      await directive.ngOnInit();

      expect((directive as any).getInput).toHaveBeenCalledWith(el.nativeElement);

      expect((directive as any).createCountElement).toHaveBeenCalled();
      expect((directive as any).getMaxLength).toHaveBeenCalledWith(control);
      expect((directive as any).input).toBe(getInputResult);
      expect((directive as any).countEl).toBe(createCountElementResult);
      expect((directive as any).maxLength).toBe(getMaxLengthResult);
    });
  });

  describe("getInput", () => {
    let getInput: any;

    beforeEach(() => {
      getInput = (directive as any).getInput.bind(directive);
      jasmine.clock().uninstall();
      jasmine.clock().install();
    });

    afterEach(function () {
      jasmine.clock().uninstall();
    });

    it("should support only ion-input and ion-textarea", async () => {
      el.nativeElement.localName = "div";
      getInput = (directive as any).getInput.bind(directive);
      await expectAsync(getInput(el.nativeElement)).toBeRejected();
    });

    it("should return the children of ion-input", (done) => {
      getInput(el.nativeElement)
        .then((child: any) => {
          expect(child).toBe(el.nativeElement.children[0]);
          done();
        })
        .catch(done.fail);
      jasmine.clock().tick(200);
    });

    it("should return the grand children of ion-textarea", (done) => {
      el.nativeElement.localName = "ion-textarea";
      const grandChild = { grandChild: 1 };
      el.nativeElement.children[0].children = [grandChild];
      getInput(el.nativeElement)
        .then((child: any) => {
          expect(child).toBe(grandChild);
          done();
        })
        .catch(done.fail);
      jasmine.clock().tick(200);
    });

    it("should return error if can't find children", (done) => {
      el.nativeElement.children = [];
      getInput(el.nativeElement).catch((err: any) => {
        expect(err).toEqual("Couldn't find input.");
        done();
      });
      jasmine.clock().tick(4000);
    });
  });

  describe("createCountElement", () => {
    let createCountElement: any;

    beforeEach(() => {
      createCountElement = (directive as any).createCountElement.bind(directive);
    });

    it("should use the renderer to add an element in the grandParent", () => {
      const countEl = { countEl: 1 };
      (renderer.createText as jasmine.Spy).and.returnValue(countEl);

      expect(createCountElement()).toBe(countEl);
      expect(renderer.appendChild).toHaveBeenCalled();
      expect(renderer.insertBefore).toHaveBeenCalled();
    });
  });

  describe("getMaxLength", () => {
    let getMaxLength: (control: any) => number | null;

    beforeEach(() => {
      getMaxLength = (directive as any).getMaxLength.bind(directive);
    });

    it("should extract the requiredLength property from validators is present", () => {
      (control.control?.validator as jasmine.Spy).and.returnValue({
        maxlength: { requiredLength: 100 },
      });

      expect(getMaxLength(control)).toEqual(100);
    });

    it("should return null if no maxlength validation error is present", () => {
      (control.control?.validator as jasmine.Spy).and.returnValue({
        required: true,
      });

      expect(getMaxLength(control)).toBeNull();
    });

    it("should return null if no validation is present", () => {
      (control.control?.validator as jasmine.Spy).and.returnValue(null);

      expect(getMaxLength(control)).toBeNull();
    });

    it("should return null if no control is present", () => {
      (control as any).control = null;

      expect(getMaxLength(control)).toBeNull();
    });
  });

  describe("ngDoCheck", () => {
    let input: any;
    let countEl: any;
    let value;

    beforeEach(() => {
      value = faker.lorem.paragraph();
      input = { value };
      countEl = { countEl: 1 };
      (directive as any).input = input;
      (directive as any).countEl = countEl;
    });

    it("should show the number of chars in input", () => {
      directive.ngDoCheck();

      expect(renderer.setValue).toHaveBeenCalledWith(countEl, input.value.length.toString());
    });

    it("should not fail with no input", () => {
      (directive as any).input = null;

      expect(() => directive.ngDoCheck()).not.toThrowError();
    });

    it("should show the maxLength, if it is present", () => {
      (directive as any).maxLength = 100;
      directive.ngDoCheck();

      expect(renderer.setValue).toHaveBeenCalledWith(
        countEl,
        `${input.value.length.toString()}/100`
      );
    });
  });
});
