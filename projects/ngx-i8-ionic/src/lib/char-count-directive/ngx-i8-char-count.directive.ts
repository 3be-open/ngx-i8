/* eslint-disable max-statements */
import { Directive, DoCheck, ElementRef, OnInit, Renderer2, Self } from "@angular/core";
import { NgControl } from "@angular/forms";

/**
 * Counts and display the number of characters on an ion-input/ion-textarea.
 * ```html
 * <ion-input formControlName="name" ngxI8CharCount></ion-input>
 * ```
 */
@Directive({
  selector: "ion-input[ngxI8CharCount], ion-textarea[ngxI8CharCount]",
})
export class NgxI8CharCountDirective implements OnInit, DoCheck {
  INPUT_WAIT_RETRIES = 20;
  INPUT_WAIT_TIME = 100; // ms
  BIG_STRING_SIZE = 100000;
  SUPPORTED_ELEMENTS = ["ion-textarea", "ion-input"];

  private input: any;

  private countEl: any;
  private maxLength?: number | null;
  constructor(
    private el: ElementRef,
    @Self() private control: NgControl,
    private renderer: Renderer2
  ) {}

  async ngOnInit() {
    this.input = await this.getInput(this.el.nativeElement);
    this.countEl = this.createCountElement();
    this.maxLength = this.getMaxLength(this.control);
  }

  private async getInput(nativeEl: any) {
    return new Promise((resolve, reject) => {
      let counter = this.INPUT_WAIT_RETRIES;
      const elementName = nativeEl.localName;
      if (this.SUPPORTED_ELEMENTS.indexOf(elementName) === -1) {
        return reject(`${elementName} is not supported (${this.SUPPORTED_ELEMENTS})`);
      }
      const interval = setInterval(() => {
        if (nativeEl.children.length === 1) {
          let child = nativeEl.children[0];
          if (elementName === "ion-textarea") {
            child = child.children[0];
          }
          clearInterval(interval);
          return resolve(child);
        } else if (counter-- === 0) {
          clearInterval(interval);
          return reject("Couldn't find input.");
        }
      }, this.INPUT_WAIT_TIME);
    });
  }

  private createCountElement() {
    const div = this.renderer.createElement("div");
    this.renderer.setAttribute(div, "class", "ngx-i8-char-count ion-text-right");
    const ionNote = this.renderer.createElement("ion-note");
    this.renderer.appendChild(div, ionNote);
    const small = this.renderer.createElement("small");
    this.renderer.appendChild(ionNote, small);
    const countEl = this.renderer.createText("?");
    this.renderer.appendChild(small, countEl);
    const parent = this.el.nativeElement.parentElement.parentElement;
    this.renderer.insertBefore(parent, div, this.el.nativeElement.parentElement.nextElementSibling);
    return countEl;
  }

  /**
   * It tries to cause a maxlength error to grab its size.
   */
  private getMaxLength(control: NgControl): number | null {
    const innerControl = control.control;
    const validator = innerControl?.validator;
    if (validator && innerControl) {
      const oldValue = control.value;
      const bigString = new Array(this.BIG_STRING_SIZE).fill("a").join();
      innerControl.patchValue(bigString);
      const maxLength = validator(innerControl)?.maxlength?.requiredLength;
      innerControl.patchValue(oldValue);
      return maxLength || null;
    }
    return null;
  }

  ngDoCheck() {
    if (this.input) {
      let result = this.input.value.length.toString();
      if (this.maxLength) {
        result += `/${this.maxLength}`;
      }
      this.renderer.setValue(this.countEl, result);
    }
  }
}
