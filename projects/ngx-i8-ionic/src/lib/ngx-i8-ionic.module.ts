import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

import { NgxI8AuthTemplateComponent } from "./auth-template/ngx-i8-auth-template.component";
import { NgxI8AutoCompleteDirective } from "./auto-complete-directive/ngx-i8-auto-complete.directive";
import { NgxI8AAutoCompletePopoverComponent } from "./auto-complete-directive/ngx-i8-auto-complete-popover.component";
import { NgxI8ButtonsDirective } from "./buttons-directive/buttons.directive";
import { NgxI8CharCountDirective } from "./char-count-directive/ngx-i8-char-count.directive";
import { NgxI8PasswordUnmaskDirective } from "./password-unmask-directive/ngx-i8-password-unmask.directive";
import { NgxI8ValidationMessagesDirective } from "./validation-messages-directive/ngx-i8-validation-messages.directive";

@NgModule({
  declarations: [
    NgxI8AuthTemplateComponent,
    NgxI8AutoCompleteDirective,
    NgxI8AAutoCompletePopoverComponent,
    NgxI8ButtonsDirective,
    NgxI8CharCountDirective,
    NgxI8PasswordUnmaskDirective,
    NgxI8ValidationMessagesDirective,
  ],
  imports: [CommonModule, FormsModule, IonicModule, ReactiveFormsModule],
  exports: [
    NgxI8AuthTemplateComponent,
    NgxI8AutoCompleteDirective,
    NgxI8ButtonsDirective,
    NgxI8CharCountDirective,
    NgxI8PasswordUnmaskDirective,
    NgxI8ValidationMessagesDirective,
  ],
})
export class NgxI8IonicModule {}
