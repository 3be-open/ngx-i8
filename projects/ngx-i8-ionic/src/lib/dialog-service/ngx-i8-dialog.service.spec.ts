import { faker } from "@faker-js/faker";
import { AlertController, ToastController } from "@ionic/angular";

import { NgxI8DialogService } from "./ngx-i8-dialog.service";

describe("DialogService", () => {
  let alertCtrl: AlertController;
  let toastCtrl: ToastController;
  let dialogSrvc: NgxI8DialogService;

  beforeEach(() => {
    alertCtrl = jasmine.createSpyObj("AlertController", ["create"]);
    toastCtrl = jasmine.createSpyObj("ToastController", ["create"]);
    dialogSrvc = new NgxI8DialogService(alertCtrl, toastCtrl);
  });

  it("should be created", () => {
    expect(dialogSrvc).toBeTruthy();
  });

  describe("toastSuccess", () => {
    it("should create a success toast", async () => {
      const message = faker.lorem.paragraph();
      const toast = { present: jasmine.createSpy() };
      (toastCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(toast));
      await dialogSrvc.toastSuccess(message);

      expect(toastCtrl.create).toHaveBeenCalledWith({
        message,
        duration: (dialogSrvc as any).defaultToastDuration,
        cssClass: "ngx-i8-toast ngx-i8-toast-success",
      });

      expect(toast.present).toHaveBeenCalled();
    });
  });

  describe("toastError", () => {
    it("should create an error toast", async () => {
      const message = faker.lorem.paragraph();
      const toast = { present: jasmine.createSpy() };
      (toastCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(toast));
      await dialogSrvc.toastError(message);

      expect(toastCtrl.create).toHaveBeenCalledWith({
        message,
        duration: (dialogSrvc as any).defaultToastDuration,
        cssClass: "ngx-i8-toast ngx-i8-toast-error",
      });

      expect(toast.present).toHaveBeenCalled();
    });
  });

  describe("confirmDelete", () => {
    it("should create a delete confirm dialog", async () => {
      const message = faker.lorem.paragraph();
      const alert = { present: jasmine.createSpy() };
      (alertCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(alert));
      const handler = jasmine.createSpy();
      await dialogSrvc.confirmDelete(message, handler);

      expect(alertCtrl.create).toHaveBeenCalled();
      const callArgs = (alertCtrl.create as jasmine.Spy).calls.argsFor(0);

      expect(callArgs[0].message).toEqual(message);
      expect(callArgs[0].buttons[0].role).toEqual("cancel");
      expect(callArgs[0].buttons[1].handler).toEqual(handler);
      expect(alert.present).toHaveBeenCalled();
    });
  });

  describe("confirm", () => {
    it("should create a confirm dialog", async () => {
      const title = faker.lorem.paragraph();
      const message = faker.lorem.paragraph();
      const alert = { present: jasmine.createSpy() };
      (alertCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(alert));
      const handler = jasmine.createSpy();
      await dialogSrvc.confirm(title, message, {}, handler);

      expect(alertCtrl.create).toHaveBeenCalled();
      const callArgs = (alertCtrl.create as jasmine.Spy).calls.argsFor(0);

      expect(callArgs[0].header).toEqual(title);
      expect(callArgs[0].message).toEqual(message);
      expect(callArgs[0].buttons[0].role).toEqual("cancel");
      expect(callArgs[0].buttons[1].handler).toEqual(handler);
      expect(alert.present).toHaveBeenCalled();
    });

    it("should create a confirm dialog with an input", async () => {
      const title = faker.lorem.paragraph();
      const message = faker.lorem.paragraph();
      const input = faker.lorem.paragraph();
      const alert = { present: jasmine.createSpy() };
      (alertCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(alert));
      const handler = jasmine.createSpy();
      const opts: any = { inputs: {} };
      opts.inputs[input] = "text";
      await dialogSrvc.confirm(title, message, opts, handler);

      expect(alertCtrl.create).toHaveBeenCalled();
      const callArgs = (alertCtrl.create as jasmine.Spy).calls.argsFor(0);

      expect(callArgs[0].header).toEqual(title);
      expect(callArgs[0].message).toEqual(message);
      expect(callArgs[0].inputs[0].type).toEqual("text");
      expect(callArgs[0].inputs[0].placeholder).toEqual(input);
      expect(callArgs[0].buttons[0].role).toEqual("cancel");
      expect(callArgs[0].buttons[1].handler).toEqual(handler);
      expect(alert.present).toHaveBeenCalled();
    });

    it("should allow rename the buttons", async () => {
      const title = faker.lorem.paragraph();
      const message = faker.lorem.paragraph();
      const alert = { present: jasmine.createSpy() };
      const cancelText = faker.lorem.word();
      const okText = faker.lorem.word();
      (alertCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(alert));
      const handler = jasmine.createSpy();
      await dialogSrvc.confirm(title, message, { buttons: { cancelText, okText } }, handler);

      expect(alertCtrl.create).toHaveBeenCalled();
      const callArgs = (alertCtrl.create as jasmine.Spy).calls.argsFor(0);

      expect(callArgs[0].header).toEqual(title);
      expect(callArgs[0].message).toEqual(message);
      expect(callArgs[0].buttons[0].role).toEqual("cancel");
      expect(callArgs[0].buttons[0].text).toEqual(cancelText);
      expect(callArgs[0].buttons[1].handler).toEqual(handler);
      expect(callArgs[0].buttons[1].text).toEqual(okText);
      expect(alert.present).toHaveBeenCalled();
    });
  });

  describe("alert", () => {
    it("should create an alert", async () => {
      const title = faker.lorem.paragraph();
      const message = faker.lorem.paragraph();
      const alert = { present: jasmine.createSpy() };
      (alertCtrl.create as jasmine.Spy).and.returnValue(Promise.resolve(alert));
      await dialogSrvc.alert(title, message);

      expect(alertCtrl.create).toHaveBeenCalled();
      const callArgs = (alertCtrl.create as jasmine.Spy).calls.argsFor(0);

      expect(callArgs[0].header).toEqual(title);
      expect(callArgs[0].message).toEqual(message);
      expect(alert.present).toHaveBeenCalled();
    });
  });

  describe("setDefaultToastDuration", () => {
    it("should update the defaultToastDuration", () => {
      const randomDuration = faker.random.number();
      dialogSrvc.setDefaultToastDuration(randomDuration);

      expect((dialogSrvc as any).defaultToastDuration).toEqual(randomDuration);
    });
  });
});
