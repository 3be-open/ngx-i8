import { Injectable } from "@angular/core";
import { AlertController, ToastController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class NgxI8DialogService {
  private defaultToastDuration = 3000;

  constructor(private alertCtrl: AlertController, private toastCtrl: ToastController) {}

  /**
   * Sets the default toast duration (3000ms)
   */
  setDefaultToastDuration(toastDurantion: number) {
    this.defaultToastDuration = toastDurantion;
  }

  /**
   * Displays a toast success message message.
   * The toast will have the ```.ngx-i8-toast-success``` css class
   */
  async toastSuccess(message: string, toastDurantion = null) {
    const toast = await this.toastCtrl.create({
      message,
      duration: toastDurantion || this.defaultToastDuration,
      cssClass: "ngx-i8-toast ngx-i8-toast-success",
    });
    return toast.present();
  }

  /**
   * Displays a toast error message message.
   * The toast will have the ```.ngx-i8-toast-error``` css class
   */
  async toastError(message: string, toastDuration = null) {
    const toast = await this.toastCtrl.create({
      message,
      duration: toastDuration || this.defaultToastDuration,
      cssClass: "ngx-i8-toast ngx-i8-toast-error",
    });
    return toast.present();
  }

  /**
   * Displays a dialog to confirm a delete operation.
   * The buttons will have the ```.ngx-i8-dialog-button-back```
   * and ```ngx-i8-dialog-button-delete``` css classes
   */
  async confirmDelete(message: string, handler: () => any) {
    const alert = await this.alertCtrl.create({
      header: "Confirmar exclusão",
      message,
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "ngx-i8-button-back",
        },
        {
          text: "Excluir",
          cssClass: "ngx-i8-button-delete",
          handler,
        },
      ],
    });
    return alert.present();
  }

  /**
   * Displays a dialog to confirm a delete operation.
   * The buttons will have the ```.ngx-i8-dialog-button-back```
   * and ```ngx-i8-dialog-button-ok``` css classes
   */
  async confirm(
    title: string,
    message: string,
    opts: {
      inputs?: { [key: string]: any };
      buttons?: {
        cancelText?: string;
        okText?: string;
        okCssClass?: string;
      };
    },
    handler: (outputs: any) => any
  ) {
    const alert = await this.alertCtrl.create({
      header: title,
      message,
      inputs: Object.entries(opts?.inputs || {}).map((e) => ({
        type: e[1],
        placeholder: e[0],
      })),
      buttons: [
        {
          text: opts?.buttons?.cancelText || "Cancelar",
          role: "cancel",
          cssClass: "ngx-i8-button-back",
        },
        {
          text: opts?.buttons?.okText || "Ok",
          cssClass: "ngx-i8-button-ok",
          handler,
        },
      ],
    });
    return alert.present();
  }

  /**en
   * Displays an alert.
   */
  async alert(title: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: title,
      buttons: ["OK"],
      message,
    });
    await alert.present();
  }
}
