import { Directive, ElementRef, EventEmitter, Input, Output, Renderer2 } from "@angular/core";
import { PopoverController } from "@ionic/angular";

import { NgxI8AAutoCompletePopoverComponent } from "./ngx-i8-auto-complete-popover.component";

export type KeyValueMap = { [key: string]: any };
export enum PopoverStates {
  READY = "READY",
  LOADING = "LOADING",
}
export interface PopoverProps {
  state: PopoverStates;
  data: KeyValueMap;
  key: string;
}
@Directive({
  selector: "ion-input[ngxI8AutoComplete]",
})
export class NgxI8AutoCompleteDirective {
  @Input("ngxI8AutoComplete")
  handler!: (key: string) => Promise<KeyValueMap>;
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output("ngxI8AutoCompleteSelected") emitter = new EventEmitter<any>();
  protected props: PopoverProps;

  popover: HTMLIonPopoverElement | null = null;
  constructor(
    protected popoverCtrl: PopoverController,
    protected el: ElementRef,
    renderer: Renderer2
  ) {
    renderer.listen(el.nativeElement, "keyup", ($event) => {
      this.onUpdate($event);
    });
    this.props = {
      state: PopoverStates.LOADING,
      data: {},
      key: "",
    };
  }

  async onUpdate($event: any) {
    if (!this.popover) {
      this.createPopover($event);
    }
    this.updatePopoverProps(this.el.nativeElement.value);
  }

  private async createPopover($event: any) {
    this.popover = await this.popoverCtrl.create({
      component: NgxI8AAutoCompletePopoverComponent,
      componentProps: {
        props: this.props,
        element: this.el,
      },
      event: $event,
      showBackdrop: false,
      keyboardClose: false,
    });
    await this.popover.present();
    this.popover.onDidDismiss().then((selected) => {
      if (selected?.data) {
        this.emitter.emit(selected.data.value);
        this.el.nativeElement.value = selected.data.key;
      }
      this.popover = null;
    });
  }

  private async updatePopoverProps(key: string) {
    this.props.state = PopoverStates.LOADING;
    this.props.key = key;
    this.props.data = await this.handler(key);
    this.props.state = PopoverStates.READY;
  }
}
