import { Component, Input } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { PopoverController } from "@ionic/angular";

import { PopoverProps, PopoverStates } from "./ngx-i8-auto-complete.directive";

@Component({
  template: ` <ion-list>
    <ion-item *ngIf="isEmpty()">Não há dados</ion-item>
    <ion-item
      *ngFor="let item of props?.data | keyvalue"
      ion-item
      (click)="dismiss(item.key)"
      [innerHtml]="makeItBold(item.key)"
    ></ion-item>
    <ion-item *ngIf="props?.state === POPOVER_STATES.LOADING"
      ><ion-note>Loading...</ion-note></ion-item
    >
  </ion-list>`,
})
export class NgxI8AAutoCompletePopoverComponent {
  POPOVER_STATES = PopoverStates;

  constructor(public popoverCtrl: PopoverController, protected sanitizer: DomSanitizer) {}
  @Input()
  props!: PopoverProps;

  isEmpty() {
    return Object.keys(this.props?.data).length === 0;
  }

  makeItBold(itemKey: string) {
    if (this.props.key.length > 0) {
      const regexp = new RegExp(`(${this.props.key})`, "i");
      return this.sanitizer.bypassSecurityTrustHtml(itemKey.replace(regexp, `<b>$1</b>`));
    }
    return itemKey;
  }

  dismiss(key: string) {
    this.popoverCtrl.dismiss({
      key,
      value: this.props.data[key],
    });
  }
}
