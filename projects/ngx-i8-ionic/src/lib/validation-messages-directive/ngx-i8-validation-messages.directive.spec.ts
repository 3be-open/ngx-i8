import { CommonModule } from "@angular/common";
import { Component, DebugElement, NO_ERRORS_SCHEMA, Renderer2 } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidationErrors,
  Validators,
} from "@angular/forms";
import { By } from "@angular/platform-browser";
import { IonicModule } from "@ionic/angular";

import { NgxI8IonicModule } from "../ngx-i8-ionic.module";

function weirdValidator(control: AbstractControl): ValidationErrors {
  return { weird: true };
}

@Component({
  template: `<form [formGroup]="formGroup">
    <ion-item>
      <ion-input
        id="regular"
        type="number"
        formControlName="regular"
        ngxI8ValidationMessages
      ></ion-input>
    </ion-item>
    <ion-item>
      <ion-input
        id="messages"
        type="number"
        formControlName="messages"
        [ngxI8ValidationMessages]="errorMessages"
      ></ion-input>
    </ion-item>
  </form>`,
})
class TestComponent {
  errorMessages: { [key: string]: string };
  formGroup: UntypedFormGroup;

  constructor() {
    const fb = new UntypedFormBuilder();
    this.formGroup = fb.group({
      regular: fb.control("", []),
      messages: fb.control("", []),
    });
    this.errorMessages = {};
  }
}

describe("NgxI8ValidationMessagesDirective", () => {
  let fixture: ComponentFixture<TestComponent>;
  let component: TestComponent;
  let regularEl: DebugElement;
  let messagesEl: DebugElement;
  let renderer: Renderer2;

  function getValidationErrorsPlaceHolderEl(element: any) {
    const placeHolder = element.parent.children.filter(
      (child: any) => child.nativeElement.className.indexOf("ngx-i8-validation-error") >= 0
    );
    console.log(placeHolder);
    return placeHolder[0];
  }

  beforeEach(() => {
    const testBed = TestBed.configureTestingModule({
      declarations: [TestComponent],
      imports: [CommonModule, FormsModule, IonicModule, NgxI8IonicModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = testBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    regularEl = fixture.debugElement.queryAll(By.css("#regular"))[0];
    messagesEl = fixture.debugElement.queryAll(By.css("#messages"))[0];
    renderer = fixture.componentRef.injector.get(Renderer2);
    spyOn(renderer, "appendChild").and.callThrough();
    spyOn(renderer, "removeChild").and.callThrough();
  });

  it("should create a placeholder", () => {
    expect(regularEl).toBeTruthy();
    expect(messagesEl).toBeTruthy();
    expect(getValidationErrorsPlaceHolderEl(regularEl)).toBeTruthy();
    expect(getValidationErrorsPlaceHolderEl(messagesEl)).toBeTruthy();
  });

  it("should have no errors while pristine", () => {
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(0);

    expect(getValidationErrorsPlaceHolderEl(messagesEl).children.length).toEqual(0);
  });

  it("should show error if the form is invalid", () => {
    component.formGroup.controls.regular.setValidators([Validators.min(5)]);
    component.formGroup.controls.regular.setValue(1);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
  });

  it("should not re-render everything if the error is the same", () => {
    // First update
    component.formGroup.controls.regular.setValidators([Validators.min(5)]);
    component.formGroup.controls.regular.setValue(1);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).toHaveBeenCalled();
    expect(renderer.removeChild).not.toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
    // Second update
    (renderer.appendChild as jasmine.Spy).calls.reset();
    component.formGroup.controls.regular.setValue(2);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).not.toHaveBeenCalled();
    expect(renderer.removeChild).not.toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
  });

  it("should re-render everything if the error changes", () => {
    // First update
    component.formGroup.controls.regular.setValidators([Validators.min(5), Validators.max(10)]);
    component.formGroup.controls.regular.setValue(1);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).toHaveBeenCalled();
    expect(renderer.removeChild).not.toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
    // Second update
    (renderer.appendChild as jasmine.Spy).calls.reset();
    component.formGroup.controls.regular.setValue(11);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).toHaveBeenCalled();
    expect(renderer.removeChild).toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
  });

  it("should clear the errors when it is fixed", () => {
    // First update
    component.formGroup.controls.regular.setValidators([Validators.min(5), Validators.max(10)]);
    component.formGroup.controls.regular.setValue(1);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).toHaveBeenCalled();
    expect(renderer.removeChild).not.toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(1);
    // Second update
    (renderer.appendChild as jasmine.Spy).calls.reset();
    component.formGroup.controls.regular.setValue(7);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(renderer.appendChild).not.toHaveBeenCalled();
    expect(renderer.removeChild).toHaveBeenCalled();
    expect(getValidationErrorsPlaceHolderEl(regularEl).children.length).toEqual(0);
  });

  it("should show common messages for min validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.min(5)]);
    component.formGroup.controls.regular.setValue(1);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("O valor tem que ser maior ou igual à 5.");
  });

  it("should show common messages for max validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.max(5)]);
    component.formGroup.controls.regular.setValue(6);
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("O valor tem que ser menor ou igual à 5.");
  });

  it("should show common messages for email validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.email]);
    component.formGroup.controls.regular.setValue("blah");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Endereço de email inválido.");
  });

  it("should show common messages for maxLength validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.maxLength(3)]);
    component.formGroup.controls.regular.setValue("blah");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Pode ter no máximo 3 caracteres.");
  });

  it("should show common messages for minLength validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.minLength(3)]);
    component.formGroup.controls.regular.setValue("az");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Tem que ter no mínimo 3 caracteres.");
  });

  it("should show common messages for pattern validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.pattern(/[0-9]+/)]);
    component.formGroup.controls.regular.setValue("az");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Formato inválido.");
  });

  it("should show common messages for required validator", () => {
    component.formGroup.controls.regular.setValidators([Validators.required]);
    component.formGroup.controls.regular.setValue("");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Não pode estar em branco.");
  });

  it("should show basic messages for unknown validator", () => {
    component.formGroup.controls.regular.setValidators([weirdValidator]);
    component.formGroup.controls.regular.setValue("");
    component.formGroup.controls.regular.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(regularEl).children[0].nativeElement.innerHTML
    ).toContain("Erro desconhecido (weird).");
  });

  it("should show override message for unknown validator", () => {
    component.errorMessages = {
      weird: "Blah",
    };
    component.formGroup.controls.messages.setValidators([weirdValidator]);
    component.formGroup.controls.regular.setValue("1");
    component.formGroup.controls.regular.markAsTouched();
    component.formGroup.controls.messages.setValue("2");
    component.formGroup.controls.messages.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(messagesEl).children[0].nativeElement.innerHTML
    ).toContain("Blah");
  });

  it("should show override message for known validator", () => {
    component.errorMessages = {
      min: "Valor muito pequeno.",
    };
    component.formGroup.controls.messages.setValidators([Validators.min(5)]);
    component.formGroup.controls.messages.setValue(1);
    component.formGroup.controls.messages.markAsTouched();
    fixture.detectChanges();

    expect(
      getValidationErrorsPlaceHolderEl(messagesEl).children[0].nativeElement.innerHTML
    ).toContain("Valor muito pequeno.");
  });
});
