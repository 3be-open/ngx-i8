import { Directive, DoCheck, ElementRef, Input, OnInit, Renderer2, Self } from "@angular/core";
import { NgControl } from "@angular/forms";

/**
 * Shows validation errors below the input. The directive accepts an key/value
 * object to transform/add messages
 *
 * It provides default messages for:
 * * email
 * * min
 * * max
 * * maxlength
 * * minlength
 * * pattern
 * * required
 *
 * ```html
 * <ion-input
 *   [ngxI8ValidationMessages]="{
 *     notMatch: 'Password not match'
 *     min: 'Value is too small'
 *   }"
 * ></ion-input>
 * ```
 */
@Directive({
  selector: "[ngxI8ValidationMessages]",
})
export class NgxI8ValidationMessagesDirective implements OnInit, DoCheck {
  @Input("ngxI8ValidationMessages") messages: any = {};

  private placeHolderEl: any;
  private errorHash: string | null = "";

  constructor(
    private el: ElementRef,
    @Self() private control: NgControl,
    private renderer: Renderer2
  ) {}

  async ngOnInit() {
    this.placeHolderEl = this.createPlaceHolderElement();
  }

  ngDoCheck() {
    if (
      !this.control.errors ||
      JSON.stringify(Object.keys(this.control.errors)) !== this.errorHash
    ) {
      this.errorHash = null;
      const childElements = this.placeHolderEl.childNodes;
      for (const child of childElements) {
        this.renderer.removeChild(this.placeHolderEl, child);
      }
      if (this.control.errors && this.control.touched) {
        for (const errorId of Object.keys(this.control.errors)) {
          this.addErrorMessage(errorId);
        }
        this.errorHash = JSON.stringify(Object.keys(this.control.errors));
      }
    }
  }

  private addErrorMessage(errorId: string) {
    const div = this.renderer.createElement("div");
    const error = this.renderer.createText(this.getMessage(errorId));
    this.renderer.appendChild(div, error);
    this.renderer.appendChild(this.placeHolderEl, div);
  }

  /**
   * @deprecated
   */
  getErrors(): string[] {
    return Object.getOwnPropertyNames(this.control.errors);
  }

  // eslint-disable-next-line complexity
  // eslint-disable-next-line max-lines-per-function
  getMessage(messageId: string): string {
    const errors = this.control.errors || {};
    if (this.messages[messageId]) {
      return this.messages[messageId];
    }
    switch (messageId) {
      case "email":
        return "Endereço de email inválido.";
      case "min":
        return `O valor tem que ser maior ou igual à ${errors.min.min}.`;
      case "max":
        return `O valor tem que ser menor ou igual à ${errors.max.max}.`;
      case "maxlength":
        return `Pode ter no máximo ${errors.maxlength.requiredLength}` + ` caracteres.`;
      case "notMatch":
        return "As senhas não são iguais.";
      case "minlength":
        return `Tem que ter no mínimo ${errors.minlength.requiredLength} ` + `caracteres.`;
      case "pattern":
        return "Formato inválido.";
      case "required":
        return "Não pode estar em branco.";
      default:
        console.error({
          validationErrorId: messageId,
          validationValue: errors[messageId],
        });
        console.log("EMIT", "unknownValidationMessage", { messageId });
        return `Erro desconhecido (${messageId}).`;
    }
  }

  private createPlaceHolderElement() {
    const div = this.renderer.createElement("div");
    this.renderer.addClass(div, "ngx-i8-validation-error");
    this.renderer.addClass(div, "validation-error");
    this.renderer.appendChild(this.el.nativeElement.parentElement, div);
    return div;
  }
}
