import { ValidatorFn } from "@angular/forms";

import { ngxI8AreEqualValidator } from "./ngx-i8-are-equal.validator";
const OTHER = "other";

function createControl(): any {
  const control: any = {
    value: null,
    parent: { get: jasmine.createSpy() },
    updateValueAndValidity: jasmine.createSpy(),
    statusChanges: {
      subscribe: jasmine.createSpy().and.callFake((fn) => (control.internalFn = fn)),
    },
    internalFn: null,
    triggerStatusChange: () => control.internalFn(),
  } as any;
  return control;
}
describe("ngxI8AreEqualValidator", () => {
  let ngxI8AreEqualValidatorFn: ValidatorFn;
  let control: any;
  let otherControl: any;

  beforeEach(() => {
    ngxI8AreEqualValidatorFn = ngxI8AreEqualValidator(OTHER);
    otherControl = createControl();
    (otherControl as any).value = undefined;
    control = createControl();
    (control as any).value = undefined;
    (control as any).parent.get.and.returnValue(otherControl);
  });

  it("should build", () => {
    expect(ngxI8AreEqualValidatorFn).toBeTruthy();
  });

  it("should do nothing if doesn't have a control", () => {
    expect(ngxI8AreEqualValidatorFn(null as any)).toBeFalsy();
  });

  it("should do nothing if the control doesn't have a parent", () => {
    expect(ngxI8AreEqualValidatorFn(control)).toBeFalsy();
  });

  it("should return null if the value of control is undefined", () => {
    (control as any).value = undefined;

    expect(ngxI8AreEqualValidatorFn(control)).toBeFalsy();
  });

  it("should return { notMatch: true } if the value of controls doesn't match", () => {
    (control as any).value = 1;
    (otherControl as any).value = 2;

    expect(ngxI8AreEqualValidatorFn(control)).toEqual({ notMatch: true });
  });

  it("should subscribe to statusChanges", () => {
    ngxI8AreEqualValidatorFn(control);

    expect(otherControl.statusChanges.subscribe).toHaveBeenCalledTimes(1);
    ngxI8AreEqualValidatorFn(control);

    expect(otherControl.statusChanges.subscribe).toHaveBeenCalledTimes(1);
  });

  it("should track the statusChanges of otherControl", () => {
    ngxI8AreEqualValidatorFn(control);

    expect(control.updateValueAndValidity).not.toHaveBeenCalled();
    otherControl.triggerStatusChange();

    expect(control.updateValueAndValidity).toHaveBeenCalled();
  });
});
