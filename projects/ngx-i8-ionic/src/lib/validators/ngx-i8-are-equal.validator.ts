import { AbstractControl, ValidatorFn } from "@angular/forms";
import { Subscription } from "rxjs";

/**
 * Checks if two inputs have the same value. It can return a notMatch validation error.
 * ```ts
 * this.form = new FormGroup({
 *   password1: new FormControl(null),
 *   password2: new FormControl(null, [
 *     ngxI8AreEqualValidator("password1"),
 *   ]),
 *  });
 * ```
 *
 * ```html
 * <ion-input
 *     id="input-password-1"
 *     formControlName="password1"
 *     [ngxI8ValidationMessages]="{ notMatch: 'As senhas digitadas não são iguais.' }"
 *   ></ion-input>
 * ```
 *
 * @param name The other formControlName
 *
 */
export function ngxI8AreEqualValidator(name: string): ValidatorFn {
  let subscription: Subscription;
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!control || !control.parent || !control.parent.get(name)) {
      return null;
    }
    const otherControl = control.parent.get(name);

    if (otherControl && !subscription) {
      subscription = otherControl.statusChanges.subscribe(() => {
        control.updateValueAndValidity();
      });
    }
    if (otherControl && control.value !== undefined && control.value !== otherControl.value) {
      return { notMatch: true };
    }
    return null;
  };
}
