import { AfterViewChecked, Directive, ElementRef, Renderer2 } from "@angular/core";

@Directive({
  selector: "ion-row[ngxI8Buttons]",
})
export class NgxI8ButtonsDirective implements AfterViewChecked {
  constructor(protected el: ElementRef, protected renderer: Renderer2) {}

  private getButtons(columns: any = null): any[] {
    const cols = columns || this.getColumns();
    const buttons = cols
      .map((column: any) => Array.from(column.childNodes))
      .reduce((result: [], childNodes: []) => {
        Array.prototype.push.apply(result, childNodes);
        return result;
      }, [])
      .filter((e: any) => e.nodeName === "ION-BUTTON");
    return Array.from(buttons);
  }

  private getColumns(): any[] {
    return Array.from(this.el.nativeElement.childNodes).filter(
      (e: any) => e.nodeName === "ION-COL"
    );
  }

  private hasButton(column: any) {
    if (column.childElementCount > 0) {
      const buttonList = this.getButtons([column]);
      return buttonList.length > 0;
    }
    return false;
  }

  ngAfterViewChecked() {
    this.getButtons().forEach((button) => this.renderer.setAttribute(button, "expand", "block"));
    const cols = this.getColumns();
    const visible: any[] = [];
    cols.forEach((col) => {
      if (!this.hasButton(col)) {
        col.setAttribute("hidden", "true");
      } else {
        col.removeAttribute("hidden");
        visible.push(col);
      }
    });
    visible.forEach((col, i) => {
      const position = 10 - i * 4;
      this.renderer.setAttribute(col, "size-sm", "12");
      this.renderer.setAttribute(col, "size-md", "2");
      this.renderer.setAttribute(
        col,
        position > 0 ? "push-sm" : "pull-sm",
        Math.abs(position).toString()
      );
    });
  }
}
