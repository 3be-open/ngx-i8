import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ButtonsDirectivePage } from "./buttons-directive.page";

describe("ButtonsDirectivePage", () => {
  let component: ButtonsDirectivePage;
  let fixture: ComponentFixture<ButtonsDirectivePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ButtonsDirectivePage],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(ButtonsDirectivePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
