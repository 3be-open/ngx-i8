import { Component } from "@angular/core";

@Component({
  selector: "app-buttons-directive",
  templateUrl: "./buttons-directive.page.html",
})
export class ButtonsDirectivePage {
  hidden = false;
  toggle() {
    this.hidden = !this.hidden;
  }
}
