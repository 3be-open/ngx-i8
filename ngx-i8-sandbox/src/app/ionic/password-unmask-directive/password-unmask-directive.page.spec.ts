import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { PasswordUnmaskDirectivePage } from "./password-unmask-directive.page";

describe("PasswordUnmaskDirectivePage", () => {
  let component: PasswordUnmaskDirectivePage;
  let fixture: ComponentFixture<PasswordUnmaskDirectivePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PasswordUnmaskDirectivePage],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(PasswordUnmaskDirectivePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
