import { Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-password-unmask-directive",
  templateUrl: "./password-unmask-directive.page.html",
})
export class PasswordUnmaskDirectivePage {
  passwordUnmaksFormGroup: UntypedFormGroup;
  constructor(fb: UntypedFormBuilder) {
    this.passwordUnmaksFormGroup = fb.group({
      password: fb.control("123456", [
        Validators.required,
        Validators.minLength(6),
      ]),
    });
  }
}
