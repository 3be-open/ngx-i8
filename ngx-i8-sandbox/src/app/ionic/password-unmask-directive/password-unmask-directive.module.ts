import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { NgxI8IonicModule } from "ngx-i8-ionic";

import { PasswordUnmaskDirectivePage } from "./password-unmask-directive.page";

const routes: Routes = [
  {
    path: "",
    component: PasswordUnmaskDirectivePage,
  },
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NgxI8IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [PasswordUnmaskDirectivePage],
})
export class PasswordUnmaskDirectivePageModule {}
