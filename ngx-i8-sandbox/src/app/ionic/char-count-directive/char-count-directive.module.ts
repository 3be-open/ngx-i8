import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { NgxI8IonicModule } from "ngx-i8-ionic";

import { CharCountPage } from "./char-count-directive.page";

const routes: Routes = [
  {
    path: "",
    component: CharCountPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxI8IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [CharCountPage],
})
export class CharCountPageModule {}
