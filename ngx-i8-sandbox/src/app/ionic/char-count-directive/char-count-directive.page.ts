import { Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-char-count-directive",
  templateUrl: "./char-count-directive.page.html",
})
export class CharCountPage {
  charCountFormGroup: UntypedFormGroup;
  constructor(fb: UntypedFormBuilder) {
    this.charCountFormGroup = fb.group({
      textWithoutMax: fb.control("12345678", [Validators.required]),
      textWithMax: fb.control("12345678", [
        Validators.required,
        Validators.maxLength(5),
      ]),
      textareaWithoutMax: fb.control("12345678\n12345678", [
        Validators.required,
      ]),
      textareaWithMax: fb.control("12345678\n12345678", [
        Validators.required,
        Validators.maxLength(10),
      ]),
    });
  }
}
