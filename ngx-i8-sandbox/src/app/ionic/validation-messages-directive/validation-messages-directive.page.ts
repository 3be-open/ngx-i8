import { Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-validation-messages-directive",
  templateUrl: "./validation-messages-directive.page.html",
})
export class ValidationMessagesDirectivePage {
  validationMessagesFormGroup: UntypedFormGroup;
  constructor(fb: UntypedFormBuilder) {
    this.validationMessagesFormGroup = fb.group({
      requiredUntouched: fb.control("", Validators.required),
      required: fb.control("", Validators.required),
      minLength: fb.control("1", Validators.minLength(5)),
      maxLength: fb.control("123456", Validators.maxLength(5)),
      min: fb.control(1, Validators.min(5)),
      max: fb.control(6, Validators.max(5)),
      email: fb.control("aol.com", Validators.email),
      pattern: fb.control("not", Validators.pattern(/^[A-Z]+$/)),
      patternCustom: fb.control("not", Validators.pattern(/^[A-Z]+$/)),
    });
    this.validationMessagesFormGroup.markAllAsTouched();
    this.validationMessagesFormGroup.controls.requiredUntouched.markAsUntouched();
  }
}
