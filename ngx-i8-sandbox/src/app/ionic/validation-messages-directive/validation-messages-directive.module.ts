import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { NgxI8IonicModule } from "ngx-i8-ionic";

import { ValidationMessagesDirectivePage } from "./validation-messages-directive.page";

const routes: Routes = [
  {
    path: "",
    component: ValidationMessagesDirectivePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxI8IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ValidationMessagesDirectivePage],
})
export class ValidationMessagesDirectivePageModule {}
