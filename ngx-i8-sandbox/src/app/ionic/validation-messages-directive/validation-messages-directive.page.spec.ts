import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ValidationMessagesDirectivePage } from "./validation-messages-directive.page";

describe("ValidationMessagesDirectivePage", () => {
  let component: ValidationMessagesDirectivePage;
  let fixture: ComponentFixture<ValidationMessagesDirectivePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ValidationMessagesDirectivePage],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(ValidationMessagesDirectivePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
