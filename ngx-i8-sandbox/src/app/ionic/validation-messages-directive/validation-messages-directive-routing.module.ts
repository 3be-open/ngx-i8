import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ValidationMessagesDirectivePage } from "./validation-messages-directive.page";

const routes: Routes = [
  {
    path: "",
    component: ValidationMessagesDirectivePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidationMessagesDirectivePageRoutingModule {}
