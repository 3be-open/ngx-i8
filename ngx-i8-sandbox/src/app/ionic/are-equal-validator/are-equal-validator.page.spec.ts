import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { AreEqualValidatorPage } from "./are-equal-validator.page";

describe("AreEqualValidatorPage", () => {
  let component: AreEqualValidatorPage;
  let fixture: ComponentFixture<AreEqualValidatorPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AreEqualValidatorPage],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(AreEqualValidatorPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
