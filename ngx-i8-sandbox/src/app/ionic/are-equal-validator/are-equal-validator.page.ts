import { Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { ngxI8AreEqualValidator } from "ngx-i8-ionic";

@Component({
  selector: "app-are-equal-validator",
  templateUrl: "./are-equal-validator.page.html",
})
export class AreEqualValidatorPage {
  areEqualValidatorFormGroup: UntypedFormGroup;
  constructor(fb: UntypedFormBuilder) {
    this.areEqualValidatorFormGroup = fb.group({
      password1: fb.control("12345678", [
        Validators.required,
        Validators.minLength(6),
      ]),
      password2: fb.control("12345678", [
        Validators.required,
        Validators.minLength(6),
        ngxI8AreEqualValidator("password1"),
      ]),
    });
  }
}
