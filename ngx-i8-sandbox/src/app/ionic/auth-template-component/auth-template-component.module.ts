import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { NgxI8IonicModule } from "ngx-i8-ionic";

import { AuthTemplateComponentPage } from "./auth-template-component.page";

const routes: Routes = [
  {
    path: "",
    component: AuthTemplateComponentPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxI8IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AuthTemplateComponentPage],
})
export class AuthTemplateComponentPageModule {}
