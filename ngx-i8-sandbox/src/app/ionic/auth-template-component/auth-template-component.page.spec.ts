import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { AuthTemplateComponentPage } from "./auth-template-component.page";

describe("AuthTemplateComponentPage", () => {
  let component: AuthTemplateComponentPage;
  let fixture: ComponentFixture<AuthTemplateComponentPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AuthTemplateComponentPage],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(AuthTemplateComponentPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
