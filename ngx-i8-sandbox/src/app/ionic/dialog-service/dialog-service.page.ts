import { Component } from "@angular/core";
import { NgxI8DialogService } from "ngx-i8-ionic";

@Component({
  selector: "app-dialog-service",
  templateUrl: "./dialog-service.page.html",
})
export class DialogServicePage {
  confirm = 0;
  confirmDelete = 0;
  constructor(public dialogSrvc: NgxI8DialogService) {}

  onConfirmButtonClick() {
    this.dialogSrvc.confirm("confirm-title", "confirm-message", {}, () => {
      this.confirm++;
    });
  }

  onConfirmDeleteButtonClick() {
    this.dialogSrvc.confirmDelete("confirm-delete-message", () => {
      this.confirmDelete++;
    });
  }
}
