import { Injectable } from "@angular/core";
import { Auth } from "@angular/fire/auth";
import { NgxI8AbstractAuthService } from "ngx-i8-angularfire";

@Injectable({ providedIn: "root" })
export class AuthService extends NgxI8AbstractAuthService {
  constructor(protected auth: Auth) {
    super(auth);
  }
}
