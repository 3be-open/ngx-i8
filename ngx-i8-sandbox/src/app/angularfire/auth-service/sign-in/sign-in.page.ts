import { Component, OnInit } from "@angular/core";
import { UntypedFormBuilder, Validators } from "@angular/forms";
import { UntypedFormGroup } from "@angular/forms";

import { AuthService } from "../auth.service";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.page.html",
})
export class SignInPage implements OnInit {
  user$;

  signInFormGroup: UntypedFormGroup;
  constructor(protected authSrvc: AuthService, fb: UntypedFormBuilder) {
    this.signInFormGroup = fb.group({
      email: fb.control("ngx-i8+test@gmail.com", Validators.required),
      password: fb.control("123456", Validators.required),
    });
  }

  ngOnInit() {
    this.user$ = this.authSrvc.getUser$();
  }

  async onSignInFormSubmit() {
    if (this.signInFormGroup.valid) {
      const email = this.signInFormGroup.value.email;
      const password = this.signInFormGroup.value.password;
      try {
        const result = await this.authSrvc.signInWithEmailAndPassword(
          email,
          password
        );
        console.log({ result });
      } catch (err) {
        switch (err.code) {
          case "auth/wrong-password":
          case "auth/user-not-found":
            alert(err.code);
            break;
          default:
            alert("LOGGED UNKNOWN ERROR TO THE CONSOLE");
            console.log({ err });
        }
      }
    }
  }

  async onSignOutButtonClick() {
    await this.authSrvc.signOut();
  }
}
