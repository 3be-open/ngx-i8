import { Component } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "../auth.service";

@Component({
  selector: "app-create-user",
  templateUrl: "./create-user.page.html",
})
export class CreateUserPage {
  createUserFormGroup: UntypedFormGroup;
  constructor(
    protected authSrvc: AuthService,
    protected router: Router,
    fb: UntypedFormBuilder
  ) {
    this.createUserFormGroup = fb.group({
      email: fb.control("", [Validators.required, Validators.email]),
      password: fb.control("", [Validators.required, Validators.minLength(6)]),
    });
  }

  async onCreateUserFormSubmit() {
    if (this.createUserFormGroup.valid) {
      const email = this.createUserFormGroup.value.email;
      const password = this.createUserFormGroup.value.password;
      try {
        const result = await this.authSrvc.createUserWithEmailAndPassword(
          email,
          password
        );
        console.log({ result });
        this.router.navigate(["/angularfire/auth-service/sign-in"]);
      } catch (err) {
        switch (err?.code) {
          case "auth/email-already-in-use":
            alert(err.code);
            break;
          default:
            alert("LOGGED UNKNOWN ERROR TO THE CONSOLE");
            console.log({ err });
        }
      }
    }
  }
}
