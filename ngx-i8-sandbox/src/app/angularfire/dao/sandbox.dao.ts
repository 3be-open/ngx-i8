import { Injectable } from "@angular/core";
import { Firestore } from "@angular/fire/firestore";
import { NgxI8AbstractGenericDao } from "ngx-i8-angularfire";

export interface Data {
  id?: string;
  name: string;
}

@Injectable({ providedIn: "root" })
export class SandboxDao extends NgxI8AbstractGenericDao<Data> {
  constructor(firestore: Firestore) {
    super(firestore, "data");
  }
}
