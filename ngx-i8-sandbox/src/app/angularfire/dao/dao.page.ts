import { Component, OnDestroy } from "@angular/core";
import { Data, SandboxDao } from "./sandbox.dao";
import { faker } from "@faker-js/faker";
import { Observable } from "rxjs";

enum ActionEnum {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
  GET = "GET",
  LIST = "LIST",
  GET$ = "GET$",
  LIST$ = "LIST$",
}

@Component({
  selector: "app-dao",
  templateUrl: "./dao.page.html",
  styleUrls: ["./dao.page.scss"],
})
export class DaoPage implements OnDestroy {
  public id: string = null;
  public actions: string[] = [];
  public next: ActionEnum = ActionEnum.CREATE;
  public ActionEnum = ActionEnum;
  public list: Data[] = null;
  public list$: Observable<Data[]> = null;
  public get: Data = null;
  public get$: Observable<Data> = null;

  constructor(protected dao: SandboxDao) {}
  async ngOnDestroy() {
    if (this.id) {
      await this.dao.delete(this.id);
    }
  }

  async onCreateButtonClick() {
    const data = { name: faker.name.firstName() + " " + faker.name.lastName() };
    const original = Object.assign({}, data);
    const result = await this.dao.save(data);
    this.id = result.id;
    this.updateNext(ActionEnum.UPDATE, { original, result });
  }

  async onUpdateButtonClick() {
    const data = await this.dao.save({
      id: this.id,
      name: faker.name.firstName() + " " + faker.name.lastName(),
    });
    this.updateNext(ActionEnum.DELETE, data);
  }

  async onDeleteButtonClick() {
    await this.dao.delete(this.id);
    this.updateNext(ActionEnum.CREATE, this.id);
  }

  private async updateNext(next: ActionEnum, data) {
    this.actions.push(`${this.next}: ${JSON.stringify(data)}`);
    this.next = next;
    if (next === ActionEnum.CREATE) {
      this.id = null;
      this.get$ = null;
    }
  }

  async onListButtonClick() {
    this.cleanData();
    this.list = await this.dao.list();
    this.actions.push(`${ActionEnum.LIST}: ${JSON.stringify(this.list.length)}`);
  }

  async onList$ButtonClick() {
    this.cleanData();
    this.list$ = this.dao.list$();
    this.actions.push(`${ActionEnum.LIST$}`);
  }

  async onGetButtonClick() {
    this.cleanData();
    this.get = await this.dao.get(this.id);
    this.actions.push(`${ActionEnum.GET}: ${this.id}`);
  }

  async onGet$ButtonClick() {
    this.cleanData();
    this.get$ = await this.dao.get$(this.id);
    this.actions.push(`${ActionEnum.GET$}`);
  }

  private cleanData() {
    this.list = null;
    this.get = null;
    this.get$ = null;
    this.list$ = null;
  }

  getListData() {
    return this.list.map((data) => JSON.stringify(data));
  }
}
