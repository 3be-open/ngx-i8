import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "angularfire/auth-service/sign-in",
    pathMatch: "full",
  },
  {
    path: "angularfire/auth-service/sign-in",
    loadChildren: () =>
      import("./angularfire/auth-service/sign-in/sign-in.module").then(
        (m) => m.SignInPageModule
      ),
  },
  {
    path: "angularfire/auth-service/create-user",
    loadChildren: () =>
      import("./angularfire/auth-service/create-user/create-user.module").then(
        (m) => m.CreateUserPageModule
      ),
  },
  {
    path: 'angularfire/dao',
    loadChildren: () => import('./angularfire/dao/dao.module').then( m => m.DaoPageModule)
  },
  {
    path: "ionic/char-count",
    loadChildren: () =>
      import("./ionic/char-count-directive/char-count-directive.module").then(
        (m) => m.CharCountPageModule
      ),
  },
  {
    path: "ionic/are-equal-validator",
    loadChildren: () =>
      import("./ionic/are-equal-validator/are-equal-validator.module").then(
        (m) => m.AreEqualValidatorPageModule
      ),
  },
  {
    path: "ionic/password-unmask-directive",
    loadChildren: () =>
      import(
        "./ionic/password-unmask-directive/password-unmask-directive.module"
      ).then((m) => m.PasswordUnmaskDirectivePageModule),
  },
  {
    path: "ionic/validation-messages-directive",
    loadChildren: () =>
      import(
        "./ionic/validation-messages-directive/validation-messages-directive.module"
      ).then((m) => m.ValidationMessagesDirectivePageModule),
  },
  {
    path: "ionic/dialog-service",
    loadChildren: () =>
      import("./ionic/dialog-service/dialog-service.module").then(
        (m) => m.DialogServicePageModule
      ),
  },
  {
    path: "ionic/auth-template-component",
    loadChildren: () =>
      import(
        "./ionic/auth-template-component/auth-template-component.module"
      ).then((m) => m.AuthTemplateComponentPageModule),
  },
  {
    path: "ionic/buttons-directive",
    loadChildren: () =>
      import("./ionic/buttons-directive/buttons-directive.module").then(
        (m) => m.ButtonsDirectivePageModule
      ),
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
