// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA17I3G6a-Y7m-h7LO8tjN_zGH4w6wouqk",
    authDomain: "ngx-i8-test.firebaseapp.com",
    projectId: "ngx-i8-test",
    storageBucket: "ngx-i8-test.appspot.com",
    messagingSenderId: "931894398029",
    appId: "1:931894398029:web:dff5aa3e157d921aaae004",
    measurementId: "G-WND2T7G2EZ",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
