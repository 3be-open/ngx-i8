module.exports = {
  extends: "../.eslintrc.js",
  ignorePatterns: [
    "node_modules/**/*"
  ],
  overrides: [
    {
      files: [
        "src/**/*.ts"
      ],
      parserOptions: {
        project: [
          "./tsconfig.app.json",
          "./tsconfig.spec.json"
        ],
        createDefaultProgram: true
      },
      rules: {
        "no-alerts": 0,
        "sonarjs/no-ignored-return": 0,
        "@angular-eslint/directive-selector": [
          "error",
          {
            type: "attribute",
            prefix: "app",
            style: "camelCase"
          }
        ],
        "@angular-eslint/component-selector": [
          "error",
          {
            type: "element",
            prefix: "app",
            style: "kebab-case"
          }
        ]
      }
    },
  ]
}
