module.exports = {
  root: true,
  ignorePatterns: ["zone-flags.ts", "node_modules/**/*"],
  overrides: [
    {
      files: ["**/*.ts"],
      parserOptions: {
        project: ["tsconfig.json"],
        createDefaultProgram: true,
      },
      plugins: [
        "eslint-plugin-jasmine",
        "eslint-plugin-prettier",
        "eslint-plugin-simple-import-sort",
      ],
      extends: [
        "plugin:@angular-eslint/ng-cli-compat",
        "plugin:@angular-eslint/ng-cli-compat--formatting-add-on",
        "plugin:@angular-eslint/recommended",
        "plugin:jasmine/recommended",
        "plugin:sonarjs/recommended",
        "plugin:prettier/recommended",
      ],
      rules: {
        "@angular-eslint/component-selector": [
          "error",
          {
            type: "element",
            prefix: "app",
            style: "kebab-case",
          },
        ],
        "@angular-eslint/directive-selector": [
          "error",
          {
            type: "attribute",
            prefix: "app",
            style: "camelCase",
          },
        ],
        "@angular-eslint/component-class-suffix": [
          "error",
          {
            suffixes: ["Page", "Component", "Modal"],
          },
        ],
        "simple-import-sort/imports": "error",
        "max-lines-per-function": ["error", { max: 35, skipBlankLines: true, skipComments: true }],
        "max-statements": ["error", 14], // TODO ignore this.logger
        "sonarjs/cognitive-complexity": ["error", 10],
        "max-lines": ["error", { max: 221, skipBlankLines: true, skipComments: true }],
        "max-depth": ["error", 3],
        "max-statements-per-line": ["error", { max: 1 }],
        "no-alert": "error",
        "no-loop-func": "error",
        "vars-on-top": "error",
        "no-cond-assign": "error",
        "@angular-eslint/no-input-rename": 0,
        "@typescript-eslint/naming-convention": [
          "warn",
          {
            selector: "variable",
            format: ["camelCase", "UPPER_CASE"],
            leadingUnderscore: "allow"
          }, {
            selector: "function",
            format: ["camelCase"],
            leadingUnderscore: "allow"
          },
        ],
        "guard-for-in": 0,
        "prefer-arrow/prefer-arrow-functions": 0,
        "no-unused-vars": ["error", { args: "none" }],
        "no-shadow": 0,
        "sonarjs/no-small-switch": 0,
        "@typescript-eslint/no-shadow": "error",
        "@typescript-eslint/member-ordering": 0,
        "@typescript-eslint/quotes": [
          "error",
          "double",
          { avoidEscape: true, allowTemplateLiterals: true },
        ],
        "jasmine/no-unsafe-spy": 0,
        "jasmine/prefer-toHaveBeenCalledWith": 0,
        "jasmine/no-spec-dupes": 0,
        "max-len": [
          "error",
          {
            code: 100,
            ignoreComments: true,
            ignorePattern: "^(import|export) \\{.*\\}.*;$",
          },
        ],
      },
    },
    {
      files: ["src/**/*.spec.ts"],
      rules: {
        "max-lines-per-function": [0],
        "max-statements": 0,
        "max-lines": 0,
        "no-loop-func": 0,
        "sonarjs/no-duplicate-string": 0, // SpyOn(object, "fn")
        "max-len": [
          "error",
          {
            code: 100,
            ignoreComments: true,
            ignorePattern: "^((import|export) \\{.*\\}.*;|[ ]+it\\(.* \\{)$",
          },
        ],
      },
    },
    {
      files: ["*.html"],
      extends: ["plugin:@angular-eslint/template/recommended"],
    },
  ],
};
